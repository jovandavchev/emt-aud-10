package com.example.emtnovaaud10;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmtNovaAud10Application {

    public static void main(String[] args) {
        SpringApplication.run(EmtNovaAud10Application.class, args);
    }

}
