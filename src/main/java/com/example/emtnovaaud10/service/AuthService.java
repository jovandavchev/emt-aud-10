package com.example.emtnovaaud10.service;

import com.example.emtnovaaud10.model.User;

public interface AuthService {
    User getCurrentUser();
    String getCurrentUserId();
}
