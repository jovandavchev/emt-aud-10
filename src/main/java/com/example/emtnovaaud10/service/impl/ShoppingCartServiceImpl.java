package com.example.emtnovaaud10.service.impl;

import com.example.emtnovaaud10.model.Product;
import com.example.emtnovaaud10.model.ShoppingCart;
import com.example.emtnovaaud10.model.User;
import com.example.emtnovaaud10.model.enumerations.CartStatus;
import com.example.emtnovaaud10.model.exceptions.ActiveShoppingCartAlraedyExists;
import com.example.emtnovaaud10.model.exceptions.ProductOutOfStockException;
import com.example.emtnovaaud10.model.exceptions.ShoppingCartIsNotActiveException;
import com.example.emtnovaaud10.repository.ShoppingCartRepository;
import com.example.emtnovaaud10.service.ProductService;
import com.example.emtnovaaud10.service.ShoppingCartService;
import com.example.emtnovaaud10.service.UserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ShoppingCartServiceImpl implements ShoppingCartService {

    private final ShoppingCartRepository shoppingCartRepository;
    private final UserService userService;
    private final ProductService productService;

    public ShoppingCartServiceImpl(ShoppingCartRepository shoppingCartRepository,
                                   UserService userService,
                                   ProductService productService) {
        this.shoppingCartRepository = shoppingCartRepository;
        this.userService = userService;
        this.productService = productService;
    }

    @Override
    public ShoppingCart createShoppingCart(String userId) {
        User user = this.userService.findById(userId);
        if (this.shoppingCartRepository.existsByUserUsernameAndStatus(userId, CartStatus.CREATED)) {
            throw new ActiveShoppingCartAlraedyExists();
        }
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.setUser(user);
        return this.shoppingCartRepository.save(shoppingCart);
    }

    @Override
    public ShoppingCart addProductToShoppingCart(String userId, Long productId) {
        ShoppingCart shoppingCart = this.getActiveShoppingCartOrCreateNew(userId);
        Product product = this.productService.findById(productId);
        List<Product> products = shoppingCart.getProducts();
        for (Product p : products) {
            if (p.getId().equals(productId)) {
                return shoppingCart;
            }
        }
        products.add(product);
//        List<Product> products = shoppingCart.getProducts();
//        products.add(product);
//        shoppingCart.setProducts(products);
        return this.shoppingCartRepository.save(shoppingCart);
    }

    @Override
    public ShoppingCart removeProductFromShoppingCart(String userId, Long productId) {
        ShoppingCart shoppingCart = this.getActiveShoppingCartOrCreateNew(userId);
        shoppingCart.setProducts(
                shoppingCart.getProducts()
                .stream()
                .filter(item -> !item.getId().equals(productId))
                .collect(Collectors.toList())
        );
        return this.shoppingCartRepository.save(shoppingCart);
    }

    private ShoppingCart getActiveShoppingCartOrCreateNew(String userId) {
        ShoppingCart shoppingCart = this.shoppingCartRepository
                .findByUserUsernameAndStatus(userId, CartStatus.CREATED);
        if (shoppingCart == null) {
            shoppingCart = new ShoppingCart();
            shoppingCart.setUser(this.userService.findById(userId));
            shoppingCart = this.shoppingCartRepository.save(shoppingCart);
        }
        return shoppingCart;
    }

    @Override
    public ShoppingCart cancelActiveShoppingCart(String userId) {
        ShoppingCart shoppingCart = this.shoppingCartRepository
                .findByUserUsernameAndStatus(userId, CartStatus.CREATED);
        if (shoppingCart == null) {
            throw new ShoppingCartIsNotActiveException();
        }
        shoppingCart.setStatus(CartStatus.CANCELED);
        return this.shoppingCartRepository.save(shoppingCart);
    }

    @Override
    @Transactional
    public ShoppingCart checkoutShoppingCart(String userId) {
        ShoppingCart shoppingCart = this.shoppingCartRepository
                .findByUserUsernameAndStatus(userId, CartStatus.CREATED);
        if (shoppingCart == null) {
            throw new ShoppingCartIsNotActiveException();
        }

        float price = 0;
        List<Product> products = shoppingCart.getProducts();

        for (Product product : products) {
            if (product.getQuantity() <= 0) {
                throw new ProductOutOfStockException(product.getId());
            }
            product.setQuantity(product.getQuantity() -1);
            price += product.getPrice();
        }

        //paymentService.pay(price)

        shoppingCart.setProducts(products);
        shoppingCart.setStatus(CartStatus.FINISHED);
        return this.shoppingCartRepository.save(shoppingCart);
    }
}
