package com.example.emtnovaaud10.service.impl;

import com.example.emtnovaaud10.model.User;
import com.example.emtnovaaud10.repository.UserRepository;
import com.example.emtnovaaud10.service.AuthService;
import org.springframework.stereotype.Service;

@Service
public class AuthServiceImpl implements AuthService {

    private final UserRepository userRepository;

    public AuthServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    @Override
    public User getCurrentUser() {
        return this.userRepository.findById("emt-user")
                .orElseGet(() -> {
                    User user = new User();
                    user.setUsername("emt-user");
                    return this.userRepository.save(user);
                });
    }

    @Override
    public String getCurrentUserId() {
        return this.getCurrentUser().getUsername();
    }
}
