package com.example.emtnovaaud10.service.impl;

import com.example.emtnovaaud10.model.User;
import com.example.emtnovaaud10.model.exceptions.UserNotFoundException;
import com.example.emtnovaaud10.repository.UserRepository;
import com.example.emtnovaaud10.service.UserService;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User findById(String username) {
        return this.userRepository.findById(username)
                .orElseThrow(() -> new UserNotFoundException(username));
    }
}
