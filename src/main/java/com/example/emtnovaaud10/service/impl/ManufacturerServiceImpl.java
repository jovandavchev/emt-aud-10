package com.example.emtnovaaud10.service.impl;

import com.example.emtnovaaud10.model.Manufacturer;
import com.example.emtnovaaud10.model.exceptions.ManufacturerNotFoundException;
import com.example.emtnovaaud10.model.exceptions.ThereAreProductWithThisManufacturerException;
import com.example.emtnovaaud10.repository.ManufacturerRepository;
import com.example.emtnovaaud10.repository.ProductRepository;
import com.example.emtnovaaud10.service.ManufacturerService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ManufacturerServiceImpl implements ManufacturerService {

    private final ManufacturerRepository manufacturerRepository;
    private final ProductRepository productRepository;

    public ManufacturerServiceImpl(ManufacturerRepository manufacturerRepository, ProductRepository productRepository) {
        this.manufacturerRepository = manufacturerRepository;
        this.productRepository = productRepository;
    }

    @Override
    public List<Manufacturer> findAll() {
        return this.manufacturerRepository.findAll();
    }

    @Override
    public Manufacturer findById(Long id) {
        return this.manufacturerRepository.findById(id)
                .orElseThrow(() -> new ManufacturerNotFoundException(id));
    }

    @Override
    public Manufacturer save(Manufacturer manufacturer) {
        return this.manufacturerRepository.save(manufacturer);
    }

    @Override
    public Manufacturer update(Long id, Manufacturer manufacturer) {
        Manufacturer updatedManufacturer = this.findById(id);
        updatedManufacturer.setAddress(manufacturer.getAddress());
        updatedManufacturer.setName(manufacturer.getName());
        return this.manufacturerRepository.save(updatedManufacturer);
    }


    @Override
    public void deleteById(Long id) {
        Manufacturer manufacturer = this.findById(id);
//        if (this.productRepository.existsByManufacturerId(id)) {
//            throw new ThereAreProductWithThisManufacturerException(id);
//        }
        if (manufacturer.getProducts().size() > 0) {
            throw new ThereAreProductWithThisManufacturerException(id);
        }
        this.manufacturerRepository.removeById(id);
    }
}
