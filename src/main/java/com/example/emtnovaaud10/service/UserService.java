package com.example.emtnovaaud10.service;

import com.example.emtnovaaud10.model.User;

public interface UserService {
    User findById(String username);
}
