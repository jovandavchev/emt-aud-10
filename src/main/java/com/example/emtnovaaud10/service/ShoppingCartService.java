package com.example.emtnovaaud10.service;

import com.example.emtnovaaud10.model.ShoppingCart;

public interface ShoppingCartService {
    ShoppingCart createShoppingCart(String userId);
    ShoppingCart addProductToShoppingCart(String userId, Long productId);
    ShoppingCart removeProductFromShoppingCart(String userId, Long productId);
    ShoppingCart cancelActiveShoppingCart(String userId);
    ShoppingCart checkoutShoppingCart(String userId);
}
