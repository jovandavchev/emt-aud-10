package com.example.emtnovaaud10.model.enumerations;

public enum  CartStatus {
    CREATED, CANCELED, FINISHED
}
