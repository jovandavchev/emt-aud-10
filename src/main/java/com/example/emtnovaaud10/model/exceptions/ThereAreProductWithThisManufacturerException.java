package com.example.emtnovaaud10.model.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class ThereAreProductWithThisManufacturerException extends RuntimeException {
    public ThereAreProductWithThisManufacturerException(Long id) {
        super("NASHA PORAKA");
    }
}
