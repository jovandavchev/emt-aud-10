package com.example.emtnovaaud10.repository;

import com.example.emtnovaaud10.model.Manufacturer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface ManufacturerRepository extends JpaRepository<Manufacturer, Long> {

    @Transactional
    Integer removeById(Long id);
}
