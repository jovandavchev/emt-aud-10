package com.example.emtnovaaud10.repository;

import com.example.emtnovaaud10.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
    boolean existsByManufacturerId(Long manufacturerId);
}
