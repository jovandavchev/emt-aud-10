package com.example.emtnovaaud10.repository;

import com.example.emtnovaaud10.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository  extends JpaRepository<User, String> {
}
